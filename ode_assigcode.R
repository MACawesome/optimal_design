rm(list=ls())

library(tidyverse)
library(haven)
library(readODS)
library(readxl)

setwd('/home/marc/School/Experimental_design/ode_assigment')

dma.dat <- read_ods('./design_4567.ods', sheet=2, col_names=FALSE)

X <- as.matrix(dma.dat)

X<- cbind(int=1, X)

t(X) %*% X

X <- as.data.frame(X)

X <- model.matrix(~ ., data=dma.dat)

XtXinv <- solve(t(X) %*% X)

## Question 4 computations
cmat <- read_excel('corrmat_4567.xlsx') %>%
    as.data.frame()
len.mat <- read_excel('CorrelationsLennert.xlsx',
                      col_names=FALSE) %>%
    as.data.frame()

rownames(cmat) <- cmat[['Effect Names']]
cmat <- cmat[, -1]
cmat <- as.matrix(cmat)
cmat <- cmat[9:ncol(cmat), 9:nrow(cmat)]

rownames(len.mat) <- len.mat[['Effect Names']]
len.mat <- as.matrix(len.mat)

28 * 28

cnt_unique <- function(cmat) {
    dc <- dim(cmat)
    dmat <- matrix(NA, nrow(cmat), ncol(cmat))
    for(j in 1:ncol(cmat)) {
        x <- cmat[, j]
        bool <- FALSE
        for(i in 1:length(x)) {
            if(x[i] == 1) {
                bool <- TRUE
            }
            if(bool) {
                dmat[i, j] <- NA
            }
            else {
                dmat[i, j] <- x[i]
            }
        }
    }
    values <- c(dmat)
    values <- values[!is.na(values)]
    cvalues <- unique(values)
    ccounts <- sapply(cvalues, function(x) {
        sum(values == x)
    })
    names(ccounts) <- round(cvalues, 3)
    return(ccounts)
}


sum(as.numeric(names(cnt_unique(cmat))) * cnt_unique(cmat))/
    sum(cnt_unique(cmat))

sum(as.numeric(names(cnt_unique(len.mat))) * cnt_unique(len.mat))/
    sum(cnt_unique(len.mat))

## Check whether counts are good



## Add a blocking factor over the fold, check orthogonality

dma.dat$block <- rep(1:20, 2)

write_ods(dma.dat, './d4567_blocks.ods')
write.csv(dma.dat, './d4567_blocks.csv', row.names=FALSE)

dma.list <- split(dma.dat, dma.dat$block)

lapply(dma.list, function(x) {
    apply(x, 2, sum)
})

dma.dat$block <- factor(dma.dat$block)

Xnew <- model.matrix(~ ., dma.dat)

det(t(Xnew) %*% Xnew)
det(t(X) %*% X)

solve(t(Xnew) %*% Xnew)

dim(X)
dim(Xnew)





