rm(list=ls())

library(tidyverse)
library(haven)
library(readODS)
library(readxl)

dma.dat <- read_ods('./design_4567.ods', sheet=2, col_names=FALSE)
len.dat <- read.table("143.txt")

len.dat <- rbind(len.dat, -1 * len.dat)
n <- nrow(len.dat)/2

colnames(len.dat) <- paste0('x', 1:ncol(len.dat))

len.dat$block <- rep(1:n, 2)

colnames(dma.dat) <- paste0('x', 1:ncol(dma.dat))

dma.dat$block <- rep(1:n, 2)


write.csv(len.dat, './143_design.csv', row.names=FALSE)
write.csv(dma.dat, './4567_design.csv', row.names=FALSE)

len_unblock.dat <- len.dat[, -ncol(len.dat)]
dma_unblock.dat <- dma.dat[, -ncol(dma.dat)]

X1 <- model.matrix(~ .^2 + I(x1^2) + I(x2^2) + I(x3^2) +
                 I(x4^2) + I(x5^2) + I(x6^2) + I(x7^2) +
                 I(x8^2), data=len_unblock.dat)

X2 <- model.matrix(~ .^2 + I(x1^2) + I(x2^2) + I(x3^2) +
                 I(x4^2) + I(x5^2) + I(x6^2) + I(x7^2) +
                 I(x8^2), data=dma_unblock.dat)

t(X1) %*% X1
