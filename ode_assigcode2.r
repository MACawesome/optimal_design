rm(list=ls())

setwd("d:/MarcandreC/Desktop/ode_assigment")

library(lme4)
library(lmerTest)
library(haven)
library(readxl)

med.dat <- read_excel("inTESTine medium experiment.xlsx", sheet=1)

str(med.dat)

med.dat$Plate <- factor(med.dat$Plate)
med.dat$O2 <- factor(med.dat$O2)
colnames(med.dat) <- gsub('-|\\.+', '_', colnames(med.dat))




med.fit1 <- lmer(LDH_baso ~ . - Plate + (1 | Plate), data=med.dat)

summary(med.fit1)

str(med.dat)

med.fit2 <- lmer(LDH_baso ~ D_valine + FBS + L_plantarum + 
                     (1 | Plate), data=med.dat)

summary(med.fit2)

confint(med.fit2)
