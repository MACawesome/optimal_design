#+LATEX_HEADER: \usepackage{parskip}
#+LATEX_HEADER: \usepackage{mathtools}
#+LATEX_HEADER: \usepackage{biblatex}
#+LATEX_HEADER: \usepackage{float}
#+LATEX_HEADER: \newcommand{\mbf}{\, \mathbf}
#+LATEX_HEADER: \addbibresource{ode_ref.bib}



* Part 1
** Question 1
   We currently have 2 designs (two team members)
** Question 2
*** What to look for
Linear effects is equivalent to main effects
See pp. 15, 21, 43 in ODE textbook. 
See pp. 9, 30-33 for alias matrix + 20, 29, 31 and 44 for related subjects

Check aliases of effects
hierarchy principle
multicollinearity?
Orthogonal array or plackett-burman design? (Number of runs is multiple of 4)
D-efficiency (not usually recommended)
Variance, standard errors of parameter estimates
Power for significance detection.

*** Answer pieces
The correlations' color map and the information matrix shows that the
linear effects estimates are uncorrelated. Thus the design is
orthogonal for the main effects. As such, the variances of these
estimates are going to be close to their minimum relatively to the
error variance. Indeed, looking at the relative standard errors of the
estimates, we see that they are all close to the minimum of
$\sqrt(1/n)=0.158$ at 0.162. 

There is also no confounding with the second-order interactions, as
shown in the Alias matrix. Since the design is orthogonal. 

Due to the hierarchy principle, it is reasonable to expect that even
if there were confounding of the linear effects with higher-order
interactions, the induced bias would be small.

The design is however not D-optimal. 

** Question 3
*** Ideas
Give equation of FI (with website link for jmp)
t(X) %*% X would be the I matrix if the variables were orthogonal, giving 
a FIC of 0 according to the equations. 
*** Answer pieces
The component of the Fractional Increase in confidence interval length
(FI) that depends on the design is $(\mbf{X}'\mbf{X})^{-1}_{ii}$,
which is a diagonal element $i$ of the inverse of the information
matrix. If the design was orthogonal, this diagonal element would
correspond to $1/n$ where n is the number of rows in the design
matrix. The FI would then be 0. As the FI is slightly higher than 0
for all linear effects (i.e. 0.026), the standard errors of the
parameter estimates are higher than they would be under an ideal
design where the determinant of the information matrix is maximised
(i.e. D-optimality).
** Question 4
We have four different correlation values: 0.056, 0.222, 0.444 and 0.
There are a total of 378 pairs of second-order interactions. Their
correlation values are distributed as such:
| 0.056 | 0.222 | 0.444 |  0 |
|   168 |   147 |    36 | 27 |
** Question 5
*** Criteria ideas
Precision of the estimates, aliasing of the main with the second and
third order effects. We should also look at the fraction of design
space plot (jmp). Not too much reliance on the summary statistics. 
Relative I-efficiency of designs 1 and 2 (see p. 90). 
** Question 6
*** Answer elements
There is now a large amount of confounding of the quadratic effects by
the unestimated second-order interactions. As such, the quadratic
effects are linear combinations of the second-order interactions. If
we pursue this model, we must assume that there is no statistically
significant second-order interaction. 

The FI for the quadratic effects are about 141 times greater than
those of the main effects. The relative std. error of the quadratic
estimates are now 4.65 times larger than would be expected under a
D-optimal design. This means that there is considerable uncertainty
about the quadratic effects. 

The correlations among quadratic effects are very mild
(i.e. 0.053). The correlations between the quadratic covariates and
the second-order interactions are 0 for effects with the following
pattern: $$ \mathrm{Cor}(x_1 * x_i, x_i^2) $$. Otherwise, they are
0.2418. 

Thus, although multicollinearity is not an issue, the design is not
appropriate for the estimation of the quadratic effects due to the
inflation of their standard errors and the confounding of these
effects with possible second-order interactions.

** Question 7
Since the design is folded by taking every row and multiplying it by
-1, the mean row for every block is 0 and the mean rows are thus equal
across the 20 blocks. Indeed, for any $a$ and $b=-a$, $a + b = 0$. 

As expected from an orthogonally blocked design, the blocking factor
does not affect the standard errors of the linear effects. Thus, the
confidence intervals of these effects are not inflated by the
inclusion of blocks in the model.

The average relative prediction variance increases from 0.095 to 0.570
with the blocked design. 
** Question 8
With the random effects, we can extract more information from the
design and have less parameters to estimate than with fixed
effects. Indeed, only the variance of the blocks must be
estimated. With the addition of the random block effects, there is yet
more uncertainty added to the quadratic effects, but we that the
reduction in mean squared error can markedly increase power.

** Question 9 
With the design maximising the determinant of the information matrix,
the loss of orthogonality for the main effects brings second-order
interactions that have tighter confidence intervals and whose
estimates are more precise. For the main effects, the cost is very
slighty increased standard errors. Since second-order effects are very
often important, we'd prefer this design based on the d-optimality
criterion in a real life scenario. Overall, this design is more
flexible at a negligeable cost for the precision of the main effects
estimates.

* Part 2
** Question 2
The fitted generalized least-square model accounts for the correlation
of observations contained in the same plate. The REML estimator is
used to obtain variance components estimates. The REML guarantees
unbiased estimates by accounting for the modeled intercepts and factor
effects (\cite[150-151]{goos_2011}). The unsignificant factors are
removed from the model. The final model contains the main effects of
/FBS/, /D-valine/ and /L.plantarum/. These are presented in Table
[[tab_maineffects_ldh]] along with the random components and summary
statistics of our model. Due to the split-plot design, the use of the
standard least-squares estimator would have yielded pessimistic
standard errors for the O2 variable. Indeed, the O2 variable is a
hard-to-change factor. The other fixed factors would have been given
optimistic standard errors, resulting in an increase of the type 2
error rate in their cases (\cite[236-238]{goos_2011}).

#+caption: Main-effects model of LDH 
#+name: tab_maineffects_ldh
#+ATTR_LATEX: :placement [H]
#+ATTR_LATEX: :align lcccc
#+ATTR_LATEX: :width .5\textwidth
|--------------------+-------------------------------------+--------+-------------+--------------|
| Fixed-effects      | Estimate [$95\% \operatorname{CI}$] |     SE | T-statistic |      P-value |
|--------------------+-------------------------------------+--------+-------------+--------------|
| Intercept          | -0.562 [-0.615, -0.510]             |  0.020 |     -27.529 |       <0.001 |
| D-valine           | 0.026 [0.003, 0.049]                |  0.011 |       2.310 |        0.030 |
| FBS                | -0.033 [-0.056, -0.010]             |  0.011 |      -2.937 |        0.007 |
| L. plantarum       | -0.025 [-0.047, -0.002]             |  0.011 |      -2.262 |        0.032 |
|--------------------+-------------------------------------+--------+-------------+--------------|
| Random-effects     | Variance component                  |     SE |         ICC | Wald p-value |
|--------------------+-------------------------------------+--------+-------------+--------------|
| Plate              | 0.0018                              | 0.0016 |       0.297 |        0.264 |
| Residual           | 0.0043                              | 0.0012 |             |              |
|--------------------+-------------------------------------+--------+-------------+--------------|
| Summary statistics |                                     |        |             |              |
|--------------------+-------------------------------------+--------+-------------+--------------|
| Adjusted $R^2$     | 0.493                               |        |             |              |
| $R^2$              | 0.534                               |        |             |              |
| RMSE               | 0.065                               |        |             |              |
|--------------------+-------------------------------------+--------+-------------+--------------|


The plate random variable is responsible for $30\%$ of the total
variance in the sample. However, the plate variance estimate has a
wide $95\%$ confidence interval ($95\%\operatorname{CI}=[-0.001,
0.005]$) due the estimate's low precision. Indeed, although the sample
contains 36 observations, only six different plates are used. The Wald
p-value in Table [[tab_maineffects_ldh]] tests whether the variance
component equals zero (\cite[193]{jmp_flm}). Here it is higher than the
critical threshold, so we keep the null hypothesis that there is
variance due to the plate random effect.

In Table [[tab_maineffects_ldh]], we observe that the fitted model has an
RMSE of 0.065 which is quite high relatively to the standard deviation
of the /LDH/ outcome ($\sigma_y=0.086$). Overall, the predictive
ability of the model seems moderate given the adjusted $R^2$ of
0.493. We do realize that an appropriate assessment of the selected
model's practical quality depends on domain-specific knowledge.

Three significant medium component main effects are detected. The
addition D-valine is associated with a less favorable outcome, whereas
additional FBS and L. plantarum are associated with a lower /LDH/
keeping everything else constant. The intercept corresponds to the
predicted /LDH/ value when the three significant covariates are kept
at the center of their respective ranges
(i.e. $\hat{\beta}_0=-0.562$).

** Question 3
Since the selected model only contains main effects, finding the
combination of medium components that minimizes LDH is not
particularily difficult. From the fixed-effect estimates in Table
[[tab_maineffects_ldh]], we see that D-valine must be as small as
possible, whereas as much FBS and L. plantarum as possible must be
contained in a given well. In the data, the true minimum and maximum
values are not provided but they corresponds respectively to the -1
and +1 levels in the design matrix. 

By setting the maximal desirability to the minimum LDH quantity and by
maximising desirability with the selected medium components on the
constraint that, for any predictor, $|x_i| \leq 1$, we obtain Figure
[[fig_profiler]]. The predicted average LDH quantity at the optimized
settings is -0.646 ($95\%\operatorname{CI}=[-0.706, -0.585]$). This is
the quantity that can be expected if D-valine, FBS and L. plantarum
are respectively set at their -1, +1 and +1 levels. Assuming that
these are the extrema of the factors' ranges, these are the levels we
recommend. Otherwise, further experimentation could assess how more
extreme settings affect the LDH quantity.

#+caption: Prediction profiler with the factors' values set to minimize the LDH quantity 
#+name: fig_profiler
#+ATTR_LATEX: :placement [H]
#+ATTR_LATEX: :width .8\textwidth
[[./prediction_profiler.png]]



\printbibliography
** Question 4
The oxygen concentration (O2) is treated as a hard-to-change factor
because it is reset only once per plate instead of for each
well. Also, there are 6 plates and 6 wells per plate, so oxygen
concentration can only be reset 6 times whereas the 14 easy-to-change
factors can be reset 6 times per plate. Furthemore, it's safe to
assume that observations within a given plate will be correlated. As
such, the plates are treated as whole-plots and O2 has a whole-plot
factor. The chosen design is a split-plot. By doing so, we account for
within-plate correlation and the limited quantity of runs for O2. With
the design, we seek to optimize the D-optimal criterion. The D-optimal
criterion minimizes the joint confidence region's volume of the effect
estimates (\cite[465]{jmp_doe}). This choice is made because the model
to be fitted contains only main effects and a relatively large number
of factors (i.e. 15). As such, we assume it is more important to know
which factors affect LDH baso then to obtain good predictions of the
outcome variable. In other words, we are screening factors for effect
size and significance. In the software, the point-exchange algorithm
is run with a 1000 random starts. This is get a split-plot design that
is as close as possible to the global D-optimality criterion maximum
(\cite[36-37]{goos_2011}).

The resulting design is very similar to the provided one. The relative
measures of efficiency indicate that our design is marginally superior
on each (see Table [[tab_releff_pp]]). Unsurprisingly, the relative
D-efficiency is larger than 1, indicating that the selected split-plot
design produces more precise main effect estimates overall. The
relative G, A and I efficiencies are also slightly larger
than 1. These respectively mean that the maximum prediction variance,
the total variance of the regression coefficients and the average
variance of prediction are lower in our proposed design (\cite[90,
466]{goos_2011, jmp_doe}).

#+caption: Relative efficiencies of the proposed against the provided design
#+name: tab_releff_pp
#+ATTR_LATEX: :placement [H]
#+ATTR_LATEX: :align lc
#+ATTR_LATEX: :width .5\textwidth
|-----------+-------|
| Criterion | Value |
|-----------+-------|
| D         | 1.033 |
| G         | 1.066 |
| A         | 1.017 |
| I         | 1.011 |
|-----------+-------|

Although a few relative standard errors are larger in the current
design, they are by just a small proportion. In Table [[tab_relest_pp]],
we see that the four effects with the minimum relative standard errors
in the provided design are a little bit larger in the proposed
design. Other main effects are more efficiently estimated in our
proposed design. The fraction of design space plot, shown in Figure
[[fsp_comparison]], demonstrates that the model's predictive ability was
preserved although we made the software favour the effects'
precision. The average prediction variances are 0.392 and 0.397 for
the proposed and provided designs.

#+caption: Relative estimation efficiencies of the proposed
#+caption: against the provided design
#+name: tab_relest_pp
#+ATTR_LATEX: :placement [H]
#+ATTR_LATEX: :align lc
#+ATTR_LATEX: :width .5\textwidth
|--------------+-------|
| Term         | Value |
|--------------+-------|
| Intercept    | 0,999 |
| O2           | 1     |
| type medium  | 0,975 |
| HEPES        | 0,987 |
| glucose      | 1,049 |
| Glutamax     | 1,014 |
| D-valine     | 1,037 |
| FBS          | 1,029 |
| ITS          | 1,023 |
| EGF          | 1,029 |
| org3x        | 1,036 |
| WNT          | 1,036 |
| r-spondin    | 1,022 |
| L. rhamnosus | 0,994 |
| L. plantarum | 0,987 |
| B. lactis    | 1,025 |
|--------------+-------|

#+caption: Fraction of design space plot with the proposed (blue) 
#+caption: and provided (purple)
#+name: fsp_comparison
#+ATTR_LATEX: :placement [H]
#+ATTR_LATEX: :width .6\textwidth
[[./fs_plot_comparison.png]]

If data collection had to be done over and a main effect model was
still of interest, we would prefer the split-plot design. Presently
however, both designs are expected to produce identical
conclusions. This conclusion is compounded by the equivalent powers of
both designs' tests of effect significances.




